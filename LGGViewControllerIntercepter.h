//
//  LGGViewControllerIntercepter.h
//  LeGuaGua
//
//  Created by wx on 15/5/24.
//  Copyright (c) 2015年 youyan. All rights reserved.
//

#import <Foundation/Foundation.h>
void LGGLog(NSString *format, ...) {
#ifdef DEBUG
    va_list argptr;
    va_start(argptr, format);
    NSLogv(format, argptr);
    va_end(argptr);
#endif
}

@interface LGGViewControllerIntercepter : NSObject

@end
