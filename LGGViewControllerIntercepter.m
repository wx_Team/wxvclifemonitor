//
//  LGGViewControllerIntercepter.m
//  LeGuaGua
//
//  Created by wx on 15/5/24.
//  Copyright (c) 2015年 youyan. All rights reserved.
//

#import "LGGViewControllerIntercepter.h"
#import <Aspects/Aspects.h>

@implementation LGGViewControllerIntercepter
+(void)load
{
    /* + (void)load 会在应用启动的时候自动被runtime调用，通过重载这个方法来实现最小的对业务方的“代码入侵” */
    [super load];
    [LGGViewControllerIntercepter sharedInstance];

}
+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    static LGGViewControllerIntercepter *sharedInstance;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[LGGViewControllerIntercepter alloc] init];
    });
    return sharedInstance;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        
        /* 在这里做好方法拦截 */
        [UIViewController aspect_hookSelector:@selector(viewDidLoad) withOptions:AspectPositionAfter usingBlock:^(id<AspectInfo>aspectInfo){
            [self viewDidLoad:[aspectInfo instance]];
        } error:NULL];
        SEL selector = NSSelectorFromString(@"dealloc");
        [UIViewController aspect_hookSelector:selector withOptions:AspectPositionBefore usingBlock:^(id<AspectInfo>aspectInfo){
            UIViewController *viewController = [aspectInfo instance];
              LGGLog(@"[%@ -----dealloc-----]", [viewController class]);
            
        }error:NULL];
        
    }
    return self;
}
- (void)viewDidLoad:(UIViewController *)viewController
{
    /* 你可以使用这个方法进行打日志，初始化基础业务相关的内容 */
    LGGLog(@"[%@ -----viewDidLoad-----]", [viewController class]);
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        viewController.edgesForExtendedLayout = UIRectEdgeNone;
        viewController.automaticallyAdjustsScrollViewInsets = NO;

    }
}


@end
