# README #
很多情况下，团队中开发人员水平参差不齐，很多同学在使用Block等情况时都没有注意打破循环引用。所以我加了这个类用来打印ViewController的创建和销毁,这样就能发现很多潜在的循环引用问题
### 使用 ###

* 拖进去就行，不需要导入，初始化

### 依赖 ###

* 因为使用Aspects来实现了AOP，所以要引入Aspects [地址](https://github.com/steipete/Aspects)